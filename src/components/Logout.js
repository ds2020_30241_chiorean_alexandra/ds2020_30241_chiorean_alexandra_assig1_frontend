import React from "react"
import { Redirect } from "react-router-dom";
import {logout} from '../service/user_authentication.js'
import {PATHS} from '../hosts.js'

const Logout = () =>{
    logout(); 
      if(window.refreshNav) window.refreshNav();
    
    return(
        <div>
            <Redirect to={PATHS.home}/>
        </div>
    );
}

export default Logout;