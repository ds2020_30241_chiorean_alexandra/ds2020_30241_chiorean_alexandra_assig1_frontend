import React from "react"

const EditableCell = ({
    value: initialValue,
    row: { index },
    column: { id },
    updateMyData, // This is a custom function that we supplied to our table instance
  }) => {
    // We need to keep and update the state of the cell normally
    const [value, setValue] = React.useState(initialValue)
  
    const onChange = e => {
      setValue(e.target.value)
    }
  
    // We'll only update the external data when the input is blurred
    const onBlur = () => {
      updateMyData(index, id, value)
    }
  
    // If the initialValue is changed external, sync it up with our state
    React.useEffect(() => {
      setValue(initialValue)
    }, [initialValue])
    

    if (!isNaN(Date.parse(initialValue)) && !Number.isFinite(initialValue)){
      let time = Date.parse(value)
      const date = new Date(time)
      let month = date.getMonth() + 1
      let month_format = month < 10 ? "0"+month : month
      let day = date.getDay() < 10 ? "0" + date.getDay() : date.getDay()
      let formatted_date = date.getFullYear() + "-" + (month_format) + "-" + day
      console.log(formatted_date)
      
        return <input type="date" value={formatted_date} onChange={onChange} onBlur={onBlur} />
    }
  
    return <input value={value} onChange={onChange} onBlur={onBlur} />
  }
  export default EditableCell;