import { React, useState } from "react";
import { useTable } from "react-table";
import "bootstrap/dist/css/bootstrap.css";
import { Button } from "react-bootstrap";
import {API } from "../../hosts.js";
import axiosInstace from "../../commons/api/axios_instance.js";
import "../../css/Table.css";

export default function Table({ columns, data, reload, editable }) {
  const [lines, setLines] = useState(data);
  const updateMyData = (rowIndex, columnID, value) => {
    setLines((old) =>
      old.map((row, index) => {
        //console.log("Values", row, index, value);
        if (index === rowIndex) {
          return {
            ...old[rowIndex],
            [columnID]: value,
          };
        }
        return row;
      })
    );
  };
  const {
    getTableProps, // table props from react-table
    getTableBodyProps, // table body props from react-table
    headerGroups, // headerGroups, if your table has groupings
    rows, // rows for the table based on the data passed
    prepareRow, // Prepare the row (this function needs to be called for each row before getting the row props)
  } = useTable({ columns, data, updateMyData });

  function getPathByTable(rowInfos){
    let path;
    switch (headerGroups[0].headers[0].Header) {
      case "Patients":
        path = API.patient + "/" + rowInfos.id;
        break;
      case "Caregivers":
        path = API.caregiver + "/" + rowInfos.id;
        break;
      case "Medication":
        path = API.medication + "/" + rowInfos.id;
        break;
      default:
        break;
    }
    return path
  }
  function deleteRow(event, rowInfos) {
    let path = getPathByTable(rowInfos);
    if (path) {
      axiosInstace
        .delete(path)
        .then((res) => {
          if (res.status === 200) {
            console.log("200");
            reload();
          } else {
            console.log(res.statusText);
          }
        })
        .catch((err) => console.log(err));
    }
  }
  function updateRow(event, rowIndex) {
    let path = getPathByTable(lines[rowIndex]);
    if(lines[rowIndex].dosage){
      lines[rowIndex]["dosage"] = parseFloat(lines[rowIndex]["dosage"])
    }
    if (path) {
      axiosInstace
        .put(path, lines[rowIndex])
        .then((res) => {
          if (res.status === 200) {
            console.log("200");
            reload();
          } else {
            console.log(res.statusText);
          }
        })
        .catch((err) => console.log(err));
    }
  }
  return (
    <table {...getTableProps()} className="table table-striped tableWidth">
      <thead>
        {headerGroups.map((headerGroup) => (
          <tr {...headerGroup.getHeaderGroupProps()} className="header">
            {headerGroup.headers.map((column) => (
              <th {...column.getHeaderProps()}>{column.render("Header")}</th>
            ))}

            <th></th>
            <th></th>
          </tr>
        ))}
      </thead>
      <tbody {...getTableBodyProps()}>
        {rows.map((row, i) => {
          prepareRow(row);
          return (
            <tr
              {...row.getRowProps()}
              
            >
              {row.cells.map((cell) => {
                return <td {...cell.getCellProps()}>{cell.render("Cell")}</td>;
              })}
              <td>
                {editable && <Button className="bg-danger border-0" onClick={(e) => deleteRow(e, row.original)}>
                  Delete
                </Button>}
              </td>
              <td>
                {editable && <Button className="customButton" onClick={(e) => updateRow(e, i)}>
                  Update
                </Button>}
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}
