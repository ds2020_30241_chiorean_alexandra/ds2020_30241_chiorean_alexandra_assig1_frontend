import React, { useMemo,  } from "react"
import Table from '../tabel/Table.js'
import EditableCell from '../tabel/EditableCell.js'
const CaregiverTable = ({data, reload}) =>{
const columns = useMemo(
    () => [          
      {
        Header: "Caregivers",
        columns: [
          {
            Header: "Name",
            accessor: "name", 
            Cell:EditableCell
          },
          {
            Header: "Gender",
            accessor: "gender"
          },
          {
            Header: "Birth date",
            accessor: "birthDate", 
            Cell:EditableCell
          },
          {
            Header: "Address",
            accessor: "address", 
            Cell:EditableCell
          }

        ]
      }
    ], 
    []
  );
  return(
    <Table columns={columns} data={data} reload={reload} editable={true}/>
  );
}
export default CaregiverTable;