import React,  { useState } from "react"
import { Button, FormGroup, FormControl, Form } from "react-bootstrap";
import axiosInstance from '../../commons/api/axios_instance.js'
import 'bootstrap/dist/css/bootstrap.css';
import '../../css/CaregiverForm.css'
import {PATHS} from '../../hosts.js'
import { useHistory } from "react-router-dom";

const CaregiverForm = () =>{
    const history = useHistory();
    const [name, setName] = useState("");
    const [birthDate, setBirthDate] = useState("");
    const [gender, setGender] = useState("");
    const [address, setAddress] = useState("");
    const [username, setUsername] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [errors, setErrors] = useState("");
    function validateForm() {
        return name.length > 0 && gender.length > 0 && address.length > 0 
        && birthDate.length > 0 && username.length > 0
        && password.length > 0 && email.length > 0;
      }
      
      function handleSubmit(event) {
        event.preventDefault();
        console.log("Handle Submit Add caregiver")
        console.log(name, birthDate, gender, address)
        axiosInstance.post('api/authentication/register', {
          username:username,
          email: email, 
          password:password,
          role:"Caregiver"})
        .then(res=>{
          console.log(res.data);
          axiosInstance.post('api/Caregivers', {
            name:name,
            gender: gender, 
            birthdate:birthDate,
            address:address,
            userId:res.data.userId}).then(res=>history.push(PATHS.doctor)).catch(err=>setErrors(err));
        })
        .catch(err =>{
          console.log("Err"+err);
        })
    
      }
    return (
        <div className="CaregiverForm">
        <Form onSubmit={handleSubmit}>
            <FormGroup controlId="name" bsSize="large">
            <Form.Label>Caregiver name</Form.Label>
            <FormControl
                autoFocus
                type="text"
                value={name}
                onChange={e => setName(e.target.value)}
            />
            </FormGroup>
            <FormGroup controlId="Gender" bsSize="large">
            <Form.Label>Gender</Form.Label>
            <FormControl
            as="select"
            defaulValue="Undefinded"
            value={gender}
            onChange={(e) => setGender(e.target.value)}
            type="text"
          >
            <option key="default">Choose...</option>
            <option key="female" value="Female">Female</option>
            <option key="male" value="Male">Male</option>
          </FormControl>
            </FormGroup>
            <FormGroup controlId="birthdate" bsSize="large">
            <Form.Label>Birth date</Form.Label>
            <FormControl
                autoFocus
                type="date"
                value={birthDate}
                onChange={e => setBirthDate(e.target.value)}
            />
            </FormGroup>
            <FormGroup controlId="address" bsSize="large">
            <Form.Label>Address</Form.Label>
            <FormControl
                autoFocus
                type="text"
                value={address}
                onChange={e => setAddress(e.target.value)}
            />
            </FormGroup>
            <label>Account informations</label>
            <FormGroup>
                <Form.Label>Username</Form.Label>
                <FormControl
                    autoFocus
                    type="text"
                    value={username}
                    onChange={e => setUsername(e.target.value)}
                />
            </FormGroup>
            <FormGroup>
                <Form.Label>Email</Form.Label>
                <FormControl
                    autoFocus
                    type="email"
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                />
            </FormGroup>
            <FormGroup controlId="password" bsSize="large">
                <Form.Label>Password</Form.Label>
                <FormControl
                    value={password}
                    onChange={e => setPassword(e.target.value)}
                    type="password"
                />
                </FormGroup>
            <Button block disabled={!validateForm()} type="submit" className="customButton">
            Add caregiver
            </Button>
        </Form>
    <label>{errors}</label>
        </div>
    );
}

export default CaregiverForm;