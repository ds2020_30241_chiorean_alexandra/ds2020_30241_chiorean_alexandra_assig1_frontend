import React, { useEffect, useState } from "react";
import { Button, FormGroup, FormControl, Form } from "react-bootstrap";
import axiosInstance from "../../commons/api/axios_instance.js";
import "bootstrap/dist/css/bootstrap.css";
import "../../css/CaregiverForm.css";
import { PATHS,API } from "../../hosts.js";
import { useHistory } from "react-router-dom";

const PatientForm = () => {
  const history = useHistory();
  const [name, setName] = useState("");
  const [birthDate, setBirthDate] = useState("");
  const [gender, setGender] = useState("");
  const [address, setAddress] = useState("");
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [errors, setErrors] = useState("");
  const [caregiver, setCaregiver] = useState("");
  const [caregivers, setCaregivers] = useState(null);
  const [medicalRecord, setMedicalRecord] = useState("");
  const [isLoaded, setLoaded] = useState(false);

  useEffect(() => {
    fetchCaregivers();
    setLoaded(true);
  }, []);

  function fetchCaregivers() {
    axiosInstance
      .get("api/Caregivers")
      .then((res) => {
        setCaregivers(res.data);
      })
      .catch((err) => setErrors(err));
  }

  function validateForm() {
    return name.length > 0 && gender.length > 0 && address.length > 0 
    && birthDate.length > 0 && medicalRecord.length > 0 && username.length > 0
    && password.length > 0 && email.length > 0;
  }

  function handleSubmit(event) {
    event.preventDefault();
    console.log("Handle Submit Add caregiver");
    console.log(name, birthDate, gender, address);
    axiosInstance
      .post("api/authentication/register", {
        username: username,
        email: email,
        password: password,
        role: "Patient",
      })
      .then((res) => {
        console.log(res.data);
        console.log({
          name: name,
            gender: gender,
            birthdate: birthDate,
            address: address,
            userId: res.data.userId,
            medicalRecord: medicalRecord,
            caregiverId:parseInt(caregiver),
            doctorId:parseInt(localStorage.getItem("id"))
        })
        axiosInstance
          .post(API.patient, 
          {
            name: name,
            gender: gender,
            birthdate: birthDate,
            address: address,
            userId: res.data.userId,
            medicalRecord: medicalRecord,
            caregiverId:parseInt(caregiver),
            doctorId:parseInt(localStorage.getItem("id"))
          })
          .then((res) => history.push(PATHS.doctor))
          .catch((err) => setErrors(err));
      })
      .catch((err) => {
        console.log("Err" + err);
      });
  }
  return (
    <div className="CaregiverForm">
      <Form onSubmit={handleSubmit}>
        <FormGroup controlId="name">
          <Form.Label>Patient name</Form.Label>
          <FormControl
            autoFocus
            type="text"
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
        </FormGroup>
        <FormGroup controlId="Gender">
          <Form.Label>Gender</Form.Label>
          <FormControl
            as="select"
            defaulValue="Undefinded"
            value={gender}
            onChange={(e) => setGender(e.target.value)}
            type="text"
          >
            <option key="default">Choose...</option>
            <option key="female" value="Female">Female</option>
            <option key="male" value="Male">Male</option>
          </FormControl>
        </FormGroup>
        <FormGroup controlId="birthdate" >
          <Form.Label>Birth date</Form.Label>
          <FormControl
            autoFocus
            type="date"
            value={birthDate}
            onChange={(e) => setBirthDate(e.target.value)}
          />
        </FormGroup>
        <FormGroup controlId="address" >
          <Form.Label>Address</Form.Label>
          <FormControl
            autoFocus
            type="text"
            value={address}
            onChange={(e) => setAddress(e.target.value)}
          />
        </FormGroup>
        {isLoaded && caregivers && (
          <FormGroup controlId="caregiver">
            <Form.Label>Caregiver</Form.Label>
            <FormControl
              as="select"
              autoFocus
              value={caregiver}
              onChange={(e) => {setCaregiver(e.target.value)}}
            >
              <option key="default">Choose caregiver...</option>
              {caregivers.map((item) => (
                <option key={item.id} value={item.id}>
                  {item.name}
                </option>
              ))}
            </FormControl>
          </FormGroup>
        )}
        <FormGroup controlId="medicalRecord" >
          <Form.Label>Medical Record</Form.Label>
          <FormControl
            autoFocus
            type="text"
            value={medicalRecord}
            onChange={(e) => setMedicalRecord(e.target.value)}
          />
        </FormGroup>
        <label>Account informations</label>
        <FormGroup>
          <Form.Label>Username</Form.Label>
          <FormControl
            autoFocus
            type="text"
            value={username}
            onChange={(e) => setUsername(e.target.value)}
          />
        </FormGroup>
        <FormGroup>
          <Form.Label>Email</Form.Label>
          <FormControl
            autoFocus
            type="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </FormGroup>
        <FormGroup controlId="password">
          <Form.Label>Password</Form.Label>
          <FormControl
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            type="password"
          />
        </FormGroup>
        <Button block disabled={!validateForm()} type="submit" className="customButton">
          Add patient
        </Button>
      </Form>
      <label>{errors}</label>
    </div>
  );
};

export default PatientForm;
