import React, { useMemo,  } from "react"
import Table from '../tabel/Table.js'

const NonEditablePatientTable = ({data}) =>{
    const columns = useMemo(
        () => [          
          {
            Header: "Patients",
            columns: [
              {
                Header: "Name",
                accessor: "name",

              },
              {
                Header: "Gender",
                accessor: "gender", 

              },
              {
                Header: "Birth date",
                accessor: "birthDate", 

              },
              {
                Header: "Medical Record",
                accessor: "medicalRecord", 
              },
            ]
          }
        ], 
        []
      );
      return(
        <Table columns={columns} data={data} editable={false}/>
      );
          
      
}
export default NonEditablePatientTable;