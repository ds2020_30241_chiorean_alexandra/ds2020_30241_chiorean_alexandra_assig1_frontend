import React from "react";
import '../../css/Table.css'
const PatientInfo = ({ data }) => {
  console.log(data);
  return (
    <div id="divInfo">
      <label id="userInfo">User info</label>
      <table class="table tableWidth">
      <tbody>
        <tr>
          <th scope="row">Name</th>
          <td>{data.name}</td>
        </tr>
        <tr>
          <th scope="row">Username</th>
          <td>{data.user.userName}</td>
        </tr>
        <tr>
          <th scope="row">Email</th>
          <td>{data.user.email}</td>
        </tr>
        <tr>
          <th scope="row">Gender</th>
          <td>{data.gender}</td>
        </tr>
        <tr>
          <th scope="row">Birthdate</th>
          <td>{data.birthDate}</td>
        </tr>
        <tr>
          <th scope="row">Address</th>
          <td>{data.address}</td>
        </tr>
      </tbody>
    </table>
    </div>
  );
};

export default PatientInfo;
