import React, { useState } from "react";
import jwt_decode from "jwt-decode";
import { Button, FormGroup, FormControl, Form } from "react-bootstrap";
import "../css/Login.css";
import { PATHS } from "../hosts.js";
import { useHistory } from "react-router-dom";
import axiosInstance from "../commons/api/axios_instance.js";

const Login = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [token, setToken] = useState(null);
  const history = useHistory();
  const [userRole, setUserRole] = useState(null);
  const [error, setError] = useState(null);

  React.useEffect(() => {
    if (token) {
      localStorage.setItem("token", token);
      axiosInstance.defaults.headers["Authorization"] = "Bearer " + token;
      console.log("AICIIII");
    } else {
      localStorage.removeItem("token");
    }
  }, [token]);

  React.useEffect(() => {
    if (userRole) {
      localStorage.setItem("role", userRole);
      switch (userRole) {
        case "Doctor":
          history.push(PATHS.doctor);
          break;
        case "Patient":
          history.push(PATHS.patient);
          break;
        case "Caregiver":
          history.push(PATHS.caregiver);
          break;
        default:
          break;
      }
      if (window.refreshNav) window.refreshNav();
    }
  }, [userRole]);

  function validateForm() {
    return email.length > 0 && password.length > 0;
  }

  function handleSubmit(event) {
    event.preventDefault();
    console.log("Handle Submit");
    console.log(email, password);
    axiosInstance
      .post("api/authentication/login", {
        email: email,
        password: password,
      })
      .then((res) => {
        if (res.status === 200) {
          setToken(res.data.token);
          var decode = jwt_decode(res.data.token);
          let userRole =
            decode[
              "http://schemas.microsoft.com/ws/2008/06/identity/claims/role"
            ];
          setUserRole(userRole);
          let userId =
            decode["http://schemas.xmlsoap.org/ws/2005/05/identity/claims/sid"];
          axiosInstance
            .get("api/" + userRole + "s/userId/" + userId)
            .then((res) => {
              console.log(res);
              localStorage.setItem("id", res.data.id);
              localStorage.setItem("username", res.data.name);
            })
            .catch((err) => console.log(err));
        }else{
          setError("Email/Password wrong")
        }
      })
      .catch((err) => {
        setError("Email/Password wrong")
      });
  }

  return (
    <div className="Login">
      <Form onSubmit={handleSubmit}>
        <FormGroup controlId="email" bsSize="large">
          <Form.Label>Email</Form.Label>
          <FormControl
            autoFocus
            type="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </FormGroup>
        <FormGroup controlId="password" bsSize="large">
          <Form.Label>Password</Form.Label>
          <FormControl
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            type="password"
          />
        </FormGroup>
        <Button block disabled={!validateForm()} type="submit">
          Login
        </Button>
        <label id="error">{error}</label>
      </Form>
      
    </div>
  );
};
export default Login;
