import React from "react";
import '../../css/CaregiverForm.css'

const Plan = ({ data }) => {
  console.log(data);
  return (
    <div id="medPlan">
      <table class="table tableWidth">
      <tbody>
        <tr>
          <th scope="row">Start date</th>
          <td>{data.startDate}</td>
        </tr>
        <tr>
          <th scope="row">End date</th>
          <td>{data.endDate}</td>
        </tr>
        <tr>
          <th scope="row">Meds</th>
          <td> {data.medsList.map((item) => (
          <div id="med">
            <p>Name: {item.medication.name}</p>
            <p>Side effects: {item.medication.sideEffects}</p>
            <p>Dosage: {item.medication.dosage}</p>
            <p>Intake period: {item.inTake}</p>
          </div>
        ))}</td>
        </tr>
      </tbody>
    </table>
    </div>
  );
};

export default Plan;
