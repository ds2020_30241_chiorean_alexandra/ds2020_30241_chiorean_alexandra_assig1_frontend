import React, { useMemo,  } from "react"
import EditableCell from "../tabel/EditableCell.js";
import Table from '../tabel/Table.js'
const MedicationTable = ({data, reload}) =>{
const columns = useMemo(
    () => [          
      {
        Header: "Medication",
        columns: [
          {
            Header: "Name",
            accessor: "name", 
            Cell:EditableCell
          },
          {
            Header: "Side effects",
            accessor: "sideEffects", 
            Cell:EditableCell
          },
          {
            Header: "Dosage(mg)",
            accessor: "dosage", 
            Cell:EditableCell
          }
        ]
      }
    ], 
    []
  );
  return(
    <Table columns={columns} data={data} reload={reload} editable={true}/>
  );
}
export default MedicationTable;