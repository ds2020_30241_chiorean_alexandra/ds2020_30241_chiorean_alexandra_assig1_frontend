import React, { useEffect, useState } from "react";
import { Button, FormGroup, FormControl, Form } from "react-bootstrap";
import axiosInstance from "../../commons/api/axios_instance.js";
import "bootstrap/dist/css/bootstrap.css";
import "../../css/CaregiverForm.css";
import { API, PATHS } from "../../hosts.js";
import { useHistory } from "react-router-dom";

const MedicationPlanForm = () => {
  const history = useHistory();
  const [patients, setPatients] = useState(null);
  const [patient, setPatient] = useState("");
  const [meds, setMeds] = useState(null);
  const [med, setMed] = useState("");
  const [inTakeRule, setRule] = useState("");
  const [startDate, setStartDate] = useState("");
  const [endDate, setEndDate] = useState("");
  const [isLoaded, setLoaded] = useState(false);
  const [selectedMeds, setSelectedMeds] = useState([]);

  useEffect(() => {
    fetchData();
    setLoaded(true);
  }, []);

  function validateForm() {
    return (
      patient.length > 0 &&
      med.length > 0 &&
      inTakeRule.length > 0 &&
      startDate.length > 0 &&
      endDate.length > 0
    );
  }

  function getMedNameById(id) {
    return ({
      medName:meds.find((x) => x.id === parseInt(id)).name, 
      inTake: selectedMeds.find((x) => x.medicationId === parseInt(id)).inTake
    });
  }

  function AddToList(e) {
    setSelectedMeds([
      ...selectedMeds,
      { medicationId: parseInt(med), inTake: inTakeRule },
    ]);
  }

  function RemoveFromList(e) {
    const items = selectedMeds.filter(
      (item) => item.medicationId !== parseInt(e.target.value)
    );
    setSelectedMeds(items);
  }

  function fetchData() {
    axiosInstance
      .get(API.patient)
      .then((result) => {
        if (result.status === 200) {
          setPatients(result.data);
        }
      })
      .catch((err) => console.log("MedicationPlan patients get" + err));
    axiosInstance
      .get(API.medication)
      .then((result) => {
        if (result.status === 200) {
          setMeds(result.data);
        }
      })
      .catch((err) => console.log("MedicationPlan medications get" + err));
  }

  function handleSubmit(event) {
    event.preventDefault();
    var plan = {
      patientId: parseInt(patient),
      medsList: selectedMeds,
      startDate: startDate,
      endDate: endDate,
    };
    axiosInstance.post(API.medicationPlan, plan).then((result) => {
      if (result.status === 200) {
        history.push(PATHS.doctor);
      } else {
        console.log("Post med plan err" + result.statusText);
      }
    });
  }
  return (
    <div className="CaregiverForm">
      <Form onSubmit={handleSubmit}>
        {isLoaded && patients && (
          <FormGroup controlId="patients">
            <Form.Label>Patient</Form.Label>
            <FormControl
              as="select"
              autoFocus
              value={patient}
              onChange={(e) => {
                setPatient(e.target.value);
                console.log(e.target.value);
              }}
            >
              <option key="default">Choose...</option>
              {patients.map((item) => (
                <option key={item.id} value={item.id}>
                  {item.name}
                </option>
              ))}
            </FormControl>
          </FormGroup>
        )}
        {isLoaded && meds && (
          <FormGroup controlId="meds">
            <Form.Label>Meds</Form.Label>
            <FormControl
              as="select"
              autoFocus
              value={med}
              onChange={(e) => {
                setMed(e.target.value);
              }}
            >
              <option key="default">Choose...</option>
              {meds.map((item) => (
                <option key={item.id} value={item.id}>
                  {item.name}
                </option>
              ))}
            </FormControl>
          </FormGroup>
        )}
        <FormGroup controlId="inTake">
          <Form.Label>In take rule(mg)</Form.Label>
          <FormControl
            autoFocus
            type="text"
            value={inTakeRule}
            onChange={(e) => setRule(e.target.value)}
          />
        </FormGroup>
        <FormGroup controlId="startDate">
          <Form.Label>Tratament start</Form.Label>
          <FormControl
            autoFocus
            type="date"
            value={startDate}
            onChange={(e) => setStartDate(e.target.value)}
          />
        </FormGroup>
        <FormGroup controlId="endDate">
          <Form.Label>Tratament stop</Form.Label>
          <FormControl
            autoFocus
            type="date"
            value={endDate}
            onChange={(e) => setEndDate(e.target.value)}
          />
        </FormGroup>
        <div id="planMeds">
          <label>Plan meds:</label>
          <div>
            {selectedMeds != null &&
              selectedMeds.map((item) => (
                <div id="medItem"key={item.medicationId}>
                  <label>Med name: </label>
                  <p>{getMedNameById(item.medicationId).medName}</p>
                  <label>InTake:</label>
                  <p>{getMedNameById(item.medicationId).inTake}</p>
                  <Button
                    className="customButton"
                    value={item.medicationId}
                    onClick={(e) => {
                      RemoveFromList(e);
                    }}
                  >
                    Delete
                  </Button>
                </div>
              ))}
          </div>
          <Button
            className="customButton"
            onClick={(e) => {
              AddToList(e);
            }}
          >
            Add Med
          </Button>
        </div>

        <Button
          className="customButton mt-3"
          block
          disabled={!validateForm()}
          type="submit"
        >
          Add medication plan
        </Button>
      </Form>
    </div>
  );
};

export default MedicationPlanForm;
