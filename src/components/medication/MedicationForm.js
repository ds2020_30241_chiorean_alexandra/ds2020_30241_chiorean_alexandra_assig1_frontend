import React, { useState } from "react";
import { Button, FormGroup, FormControl, Form } from "react-bootstrap";
import axiosInstance from "../../commons/api/axios_instance.js";
import "bootstrap/dist/css/bootstrap.css";
import "../../css/CaregiverForm.css";
import { PATHS, API } from "../../hosts.js";
import { useHistory } from "react-router-dom";

const MedicationForm = () => {
  const history = useHistory();
  const [name, setName] = useState("");
  const [sideEffects, setSideEffects] = useState("");
  const [dosage, setDosage] = useState("");
  const [errors, setErrors] = useState("");

  function validateForm() {
    return dosage.length > 0 && sideEffects.length > 0 && name.length > 0;
  }

  function handleSubmit(event) {
    event.preventDefault();
    console.log(dosage)
    axiosInstance
      .post(API.medication, {
        name:name, 
        sideEffects:sideEffects, 
        dosage:parseFloat(dosage)
    })
      .then((res) => history.push(PATHS.doctor))
      .catch((err) => setErrors(err));
  }
  return (
    <div className="CaregiverForm">
      <Form onSubmit={handleSubmit}>
        <FormGroup controlId="name">
          <Form.Label>Med name</Form.Label>
          <FormControl
            autoFocus
            type="text"
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
        </FormGroup>
        <FormGroup controlId="sideEffects">
          <Form.Label>Side effects</Form.Label>
          <FormControl
            value={sideEffects}
            onChange={(e) => setSideEffects(e.target.value)}
            type="text"
          />
        </FormGroup>
        <FormGroup controlId="dosage">
          <Form.Label>Dosage(mg)</Form.Label>
          <FormControl
            autoFocus
            type="text"
            value={dosage}
            onChange={(e) => setDosage(e.target.value)}
          />
        </FormGroup>

        <Button block className="customButton" disabled={!validateForm()} type="submit">
          Add medication
        </Button>
      </Form>
      <label>{errors}</label>
    </div>
  );
};

export default MedicationForm;
