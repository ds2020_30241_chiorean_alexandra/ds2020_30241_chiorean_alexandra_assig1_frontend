import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.css";
import "../css/home_page.css";
import jwt_decode from "jwt-decode";
import { Navbar, NavItem, NavLink, NavbarBrand } from "reactstrap";
import {useHistory} from "react-router-dom"
const NavigationBar = () => {
  const [refreshNav, setRefreshNav] = useState(1);
  window.refreshNav = () => {
    setRefreshNav(refreshNav+1);
  }

  function IsLoggedIn() {
    if (localStorage.getItem("token")) {
      return (
        <div id="userLogged">
          <label>Welcome</label>
          <p>{localStorage.getItem("username") ? ", "+localStorage.getItem("username"):""}!</p>
          <NavLink href="/logout">Logout</NavLink>
        </div>
      );
    } else {
      return <NavLink href="/login">Login</NavLink>;
    }
  }
  let role;
  if (localStorage.getItem("token")) {
    var decode = jwt_decode(localStorage.getItem("token"));
    role =
      decode["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"];
  }
  console.log("Role" + role);
  return (
    <div>
      <Navbar className="container-fluid navBarStyle">
        <NavbarBrand href="/">Medical Platform</NavbarBrand>
        {role === "Doctor" && (
          <div className="inlineElements">
            <NavLink href="/doctor/">View patients</NavLink>
            <NavLink href="/doctor/add/patient">Add new patient</NavLink>
            <NavLink href="/doctor/add/medication">Add new medication</NavLink>
            <NavLink href="/doctor/add/caregiver">Add new caregiver</NavLink>
            <NavLink href="/doctor/add/medicationPlan">
              Add new medication plan
            </NavLink>
          </div>
        )}
        {role === "Caregiver" && (
          <div className="inlineElements">
            <NavLink href="/caregiver/">View patients</NavLink>
          </div>
        )}
        {role === "Patient" && (
          <div className="inlineElements">
            <NavLink href="/patient/">View personal infos</NavLink>
          </div>
        )}
        <NavItem className="navItem ml-auto">
          <IsLoggedIn />
        </NavItem>
      </Navbar>
    </div>
  );
};
export default NavigationBar;
