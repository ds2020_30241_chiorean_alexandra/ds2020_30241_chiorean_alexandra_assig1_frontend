import React, { useEffect, useState } from "react";
import axiosInstance from "../commons/api/axios_instance.js";
import NonEditablePatientTable from '../components/patients/NonEditablePatientTable.js'
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';
import {Card} from "react-bootstrap"
import "../css/CaregiverForm.css"

const Caregiver = () => {
  localStorage.getItem("Id");
  const [patients, setPatients] = useState([]);
  const [isLoaded, setLoaded] = useState(false);
  const [counter, setCounter] = useState(0);
  const [hubConnection, setConnection] = useState(null);
  const [notifications, setNotifications] = useState([]);
  useEffect(() => {
      if(localStorage.getItem("id")){
        fetchData();

      }else{
        setCounter(counter + 1)
      }
  }, [counter]);
 useEffect(()=>{
  const createHubConnection = async () => {

    // Build new Hub Connection, url is currently hard coded.
    const hubConnect = new HubConnectionBuilder()
        .withUrl("https://medicalplatform-dotnetapi.herokuapp.com/notification")
        .build();
    try {
        await hubConnect.start()
        console.log('Connection successful!')

        // Bind event handlers to the hubConnection.
        hubConnect.on('ReceiveMessage', (notification) => {
            
            if (notification.user == localStorage.getItem("id")){
              console.log(notification, localStorage.getItem("id"));
              setNotifications(m => [...m, notification]);
            }
            
        })
    }
    catch (err) {
        alert(err);
        console.log('Error while establishing connection: ' + { err })
    }
    setConnection(hubConnect);

}

createHubConnection();

 }, [])
  function fetchData() {
    setLoaded(true);
    axiosInstance
      .get("api/Patients/caregiverId/" + localStorage.getItem("id"))
      .then((res) => {
        console.log(res);
        res.data.forEach(element => {
          console.log(element);
          element.medicationPlans.map(med=>{med = med.name + " " + med.inTake})
          console.log(element)
        });
        setPatients(res.data);
      })
      .catch((err) => {
        console.log("Could not connect");
      });
  }
  return (
    
    <div className="columns">
    <div id="patients-table">
    {isLoaded && patients && <NonEditablePatientTable data={patients}/>}
    </div>
    
    <div id="notifications"> 
    <label>Notifications</label>
    {notifications && notifications.map((item) => (
                <Card className="notificationCard">
                  <Card.Body>{item.message}</Card.Body>
                </Card>
              ))
    }
    </div>
   
   
    </div>
    
  );
};

export default Caregiver;
