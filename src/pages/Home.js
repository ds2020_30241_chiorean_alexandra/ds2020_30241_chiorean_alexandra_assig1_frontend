import React from "react";
import "bootstrap/dist/css/bootstrap.css";
import "../css/home_page.css";
import logo from '../logo.png'
import {Image} from "react-bootstrap"

const Home = () => {
  return (
    
    <div className="homePageDiv">
        <label id="title">Medical Platform</label>
        <Image id="logo" src={logo}/>
    </div>
  );
};

export default Home;
