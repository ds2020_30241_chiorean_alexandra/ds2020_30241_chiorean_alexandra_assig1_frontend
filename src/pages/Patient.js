import React, { useEffect, useState } from "react";
import Plan from "../components/medication/Plan.js";
import axiosInstance from "../commons/api/axios_instance.js";
import PatientInfo from "../components/patients/PatientInfo.js";

const Patient = () => {
  const [medicationPlans, setMedicationPlans] = useState([]);
  const [infos, setInfos] = useState(null);
  const [isLoaded, setLoaded] = useState(false);
  const [counter, setCounter] = useState(0);
  useEffect(() => {
    if(localStorage.getItem("id")){
      fetchData();
      setLoaded(true);
    }else{
      setCounter(counter + 1)
    }
}, [counter]);

  function fetchData() {
    axiosInstance
      .get("api/MedicationPlans/patientId/" + localStorage.getItem("id"))
      .then((res) => {
        console.log(res);
        setMedicationPlans([res.data]);
      })
      .catch((err) => {
        console.log("Could not connect");
      });
    axiosInstance
      .get("api/Patients/" + localStorage.getItem("id"))
      .then((res) => {
        console.log(res);
        setInfos([res.data]);
      })
      .catch((err) => {
        console.log("Could not connect");
      });
  }
  return (
    <div id="medications">
      {isLoaded && infos && infos.map((info) => <PatientInfo data={info} />)}
      <div id="for-med-plans">
      <label id="userInfo">Medication plans</label>
        {isLoaded &&
          medicationPlans[0] &&
          medicationPlans[0].map((plan) => <Plan data={plan} />)}
      </div>
    </div>
  );
};

export default Patient;
