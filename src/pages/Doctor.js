import React, { useEffect, useState } from "react";
import PatientTable from "../components/patients/PatientTable.js";
import CaregiverTable from "../components/caregiver/CaregiverTable.js";
import MedicationTable from '../components/medication/MedicationTable.js'
import axiosInstance from "../commons/api/axios_instance.js";

const Doctor = () => {
  const [patientsData, setPatientsData] = useState(null);
  const [caregiversData, setCaregiversData] = useState(null);
  const [medicationData, setMedicationData] = useState(null);
  const [isLoaded, setLoaded] = useState(false);
  const [counter, setCounter] = useState(0);
  useEffect(() => {
    if(localStorage.getItem("id")){
      fetchData();
      setLoaded(true);
    }else{
      setCounter(counter + 1)
    }
    
  }, [counter]);

  function fetchData() {
    axiosInstance
      .get("api/Patients/doctorId/"+localStorage.getItem("id"))
      .then((res) => {
        console.log(res);
        setPatientsData(res.data);
      })
      .catch((err) => {
        console.log("Could not connect");
      });
    axiosInstance
      .get("api/Caregivers")
      .then((res) => {
        console.log(res);
        setCaregiversData(res.data);
      })
      .catch((err) => {
        console.log("Could not connect");
      });
      axiosInstance
      .get("api/Medications")
      .then((res) => {
        console.log(res);
        setMedicationData(res.data);
      })
      .catch((err) => {
        console.log("Could not connect");
      });
  }
  return (
    <div>
      {isLoaded && patientsData && <PatientTable data={patientsData} reload ={fetchData} />}
      {isLoaded && caregiversData && <CaregiverTable data={caregiversData} reload = {fetchData} />}
      {isLoaded && medicationData && <MedicationTable data={medicationData} reload = {fetchData} />}
    </div>
  );
};

export default Doctor;
