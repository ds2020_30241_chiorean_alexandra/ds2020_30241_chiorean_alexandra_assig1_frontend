import axios from 'axios';

const axiosInstace =  axios.create({
    baseURL:'https://medicalplatform-dotnetapi.herokuapp.com/'
});

export default axiosInstace;