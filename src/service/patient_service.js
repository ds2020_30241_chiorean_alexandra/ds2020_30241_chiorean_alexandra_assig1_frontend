import {HOST} from '../hosts.js'
import RestApiClient from "../commons/api/rest-client.js"

const endpoint = {
    getPatients:'/api/Patients'
}

export function getPatients(callback) {
    let request = new Request(HOST.backend_api + endpoint.getPatients , {
        method: 'GET',
        headers: { 'Authorization': 'Bearer "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiQnVuYSIsImp0aSI6IjU5MDQ5N2ZhLThjYWUtNGE0My04NGRkLTQ3MWQwNWRmYWFhNCIsImh0dHA6Ly9zY2hlbWFzLm1pY3Jvc29mdC5jb20vd3MvMjAwOC8wNi9pZGVudGl0eS9jbGFpbXMvcm9sZSI6IkRvY3RvciIsImV4cCI6MTYwMzc0MzU5NX0.fGFSEWTCDkIElQ0KB0B0WSeKZ7lCLWqlcSmzvLGM-_E' },
        
    });

    RestApiClient.performRequest(request, callback);
}