import {HOST} from '../hosts.js'
import RestApiClient from "../commons/api/rest-client.js"

const endpoint = {
    login:'/api/authentication/login'
}

export function login(username, password, callback) {
    let request = new Request(HOST.backend_api + endpoint.login , {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ "email":username, "password":password })
    });

    RestApiClient.performRequest(request, callback);
}
    
export function logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('token');
        localStorage.removeItem('id');
        localStorage.removeItem('role');
        localStorage.removeItem('username');
        
    }
    



