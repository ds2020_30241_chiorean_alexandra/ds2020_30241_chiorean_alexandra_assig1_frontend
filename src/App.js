import "bootstrap/dist/css/bootstrap.css";
import "./css/home_page.css";
import NavigationBar from "./components/NavigationBar.js";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Doctor from "./pages/Doctor.js";
import Patient from "./pages/Patient.js";
import Caregiver from "./pages/Caregiver.js";
import Login from "./components/Login.js";
import Logout from "./components/Logout.js";
import PatientForm from "./components/patients/PatientForm.js";
import CaregiverForm from "./components/caregiver/CaregiverForm";
import MedicationForm from "./components/medication/MedicationForm";
import MedicationPlanForm from "./components/medication/MedicationPlanForm";
import { PATHS } from "./hosts.js";
import { Redirect } from "react-router-dom";
import Home from "./pages/Home";
import { Alert } from "react-bootstrap";

const App = () => {
  return (
    <div>
      <Router>
        <div>
          <NavigationBar />
          <Switch>
            <Route exact path="/login" render={() => <Login />} />
            <Route exact path="/logout" render={() => <Logout />} />
            <Route
              exact
              path="/doctor"
              render={() =>
                localStorage.getItem("role") === "Doctor" ? (
                  <Doctor />
                ) : localStorage.getItem("role") === null ? (
                  <Redirect to="/login" />
                ) : (
                  <Redirect to="/accessDenied"/>
                )
              }
            />
            <Route
              exact
              path="/doctor/add/patient"
              render={() =>
                localStorage.getItem("role") === "Doctor" ? (
                  <PatientForm />
                ) : localStorage.getItem("role") === null ? (
                  <Redirect to="/login" />
                ) : (
                  <Redirect to="/accessDenied"/>
                )
              }
            />
            <Route
              exact
              path="/doctor/add/caregiver"
              render={() =>
                localStorage.getItem("role") === "Doctor" ? (
                  <CaregiverForm />
                ) : localStorage.getItem("role") === null ? (
                  <Redirect to="/login" />
                ) : (
                  <Redirect to="/accessDenied"/>
                )
              }
            />
            <Route exact path="/accessDenied" render={()=> <Alert variant="danger">Access denied</Alert>} />
            <Route
              exact
              path="/doctor/add/medication"
              render={() =>
                localStorage.getItem("role") === "Doctor" ? (
                  <MedicationForm />
                ) : localStorage.getItem("role") === null ? (
                  <Redirect to="/login" />
                ) : (
                  <Redirect to="/accessDenied"/>
                )
              }
            />
            <Route
              exact
              path="/doctor/add/medicationPlan"
              render={() =>
                localStorage.getItem("role") === "Doctor" ? (
                  <MedicationPlanForm />
                ) : localStorage.getItem("role") === null ? (
                  <Redirect to="/login" />
                ) : (
                  <Redirect to="/accessDenied"/>
                )
              }
            />
            <Route
              exact
              path={PATHS.patient}
              render={() =>
                localStorage.getItem("role") === "Patient" ? (
                  <Patient />
                ) : localStorage.getItem("role") === null ? (
                  <Redirect to="/login" />
                ) : (
                  <Redirect to="/accessDenied"/>
                )
              }
            />
            <Route
              exact
              path={PATHS.caregiver}
              render={() =>
                localStorage.getItem("role") === "Caregiver" ? (
                  <Caregiver />
                ) : localStorage.getItem("role") === null ? (
                  <Redirect to="/login" />
                ) : (
                  <Redirect to="/accessDenied"/>
                )
              }
            />
            <Route exact path={PATHS.home} render={() => <Home />} />
          </Switch>
        </div>
      </Router>
    </div>
  );
};

export default App;
