export const HOST = {
    backend_api: 'https://medicalplatform-dotnetapi.herokuapp.com/',
};

export const PATHS = {
    doctor:'/doctor',
    patient:'/patient',
    caregiver:'/caregiver',
    home:'/'
}

export const API ={
    doctor:'/api/Doctors',
    patient:'/api/Patients', 
    caregiver:'/api/Caregivers', 
    medication:'/api/Medications',
    medicationPlan:'/api/MedicationPlans'
}